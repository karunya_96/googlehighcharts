$(document).ready(function () {
    var option = {
        container: ".Piechart1",
        marginTop: 20,
        marginRight: 20,
        marginBottom: 30,
        marginLeft: 80,
        height: 400,

    };
    function Piechart1(options, ChartType) {
        $(options.container).empty();
        if (options) {
            options.container = options.container ? options.container : "body";
            options.width = options.width ? options.width : $(options.container).width() / 3.05;
            options.height = options.height ? options.height : 300;
            options.marginTop = options.marginTop ? options.marginTop : 30;
            options.marginBottom = options.marginBottom ? options.marginBottom : 30;
            options.marginRight = options.marginRight ? options.marginRight : 20;
            options.marginLeft = options.marginLeft ? options.marginLeft : 50;

        }

        margin = { top: options.marginTop, right: options.marginRight, bottom: options.marginBottom, left: options.marginLeft }

        var width = options.width - options.marginRight - options.marginLeft,
            height = options.height - options.marginTop - options.marginBottom,
            radius = 90;

        var data = [
            { count: 33.3, percentage: 33.3, color: '#0082da' },
            { count: 66.3, percentage: 66.3, color: '#06243b' }
        ];
        var z = d3.scaleOrdinal()
            .range(["#0082da", "#06243b"]);

        var arc = d3.arc()
            .outerRadius(radius - 10)
            .innerRadius(0);
        var pie = d3.pie()
            .sort(null)
            .value(function (d) {
                return d.count;
            });
        var svg = d3.select("body")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(140,120)");
        var g = svg.selectAll(".arc")
            .data(pie(data))
            .enter().append("g");
        var keys = ["Male", "Female"];

        svg.append("g")
            .append("text")
            .attr("class", "view")
            .attr("y", height + 10)
            .attr("transform", "translate(-135,-15)")
            .attr("text-anchor", "start")
            .text("View");

        svg.append("g")
            .append("text")
            .attr("class", "toplabel")
            .attr("x", 2)
            .attr("y", 1)
            .attr("transform", "translate(-80,-100)")
            .text("Pie Alternative Syntax");

        h = $("body").width()
        console.log(h)
        if (ChartType == "GoogleCharts") {
            var size = 18
            svg.selectAll("legend")
                .data(keys)
                .enter()
                .append("rect")
                .attr("x", h / 8)
                .attr("y", function (d, i) { return i * (size + 5) })
                .attr("width", size)
                .attr("height", size)
                .style("fill", function (d) { return z(d) })

            // Add one dot in the legend for each name.
            svg.selectAll("legendlabel")
                .data(keys)
                .enter()
                .append("text")
                .attr("x", h / 8 + size * 1.2)
                .attr("y", function (d, i) { return i * (size + 5) + (size / 2) })
                .text(function (d) { return d })
                .attr("text-anchor", "left")
                .style("alignment-baseline", "middle")

        }

        if (ChartType == "HighCharts") {
            var size = 20
            svg.selectAll("legend")
                .data(keys)
                .enter()
                .append("rect")
                .attr("x", h / 8)
                .attr("y", function (d, i) { return i * (size + 0.5) })
                .attr("width", size + 1)
                .attr("height", size - 2)
                .style("fill", function (d) { return z(d) })

            svg.append("rect")
                .attr("x", h / 8.35)
                .attr("y", function (d, i) { return -10 + i * (size + 0.5) })
                .attr("rx", 10)
                .attr("rx", "10")
                .attr("width", 90)
                .attr("height", 60)
                .attr("fill", "none")
                .attr("stroke-width", "0.2px")
                .attr("stroke", "black");

            // Add one dot in the legend for each name.
            svg.selectAll("legendlabel")
                .data(keys)
                .enter()
                .append("text")
                .attr("x", h / 8 + size * 1.2)
                .attr("y", function (d, i) { return i * (size + 2) + (size / 2) })
                .text(function (d) { return d })
                .attr("text-anchor", "left")
                .style("alignment-baseline", "middle")

        }

        g.append("path")
            .attr("d", arc)
            .style("fill", function (d, i) {
                return d.data.color;
            })
            .attr("stroke", "white")
            .attr("stroke-width", 1)

        g.append("text")
            .attr("transform", function (d) {
                var _d = arc.centroid(d);
                _d[0] *= 1.2;	//multiply by a constant factor
                _d[1] *= 1.2;	//multiply by a constant factor
                return "translate(" + _d + ")";
            })
            //.attr("dy", ".50em")
            .style("text-anchor", "middle")
            .style("fill", "white")
            .text(function (d) {
                if (d.data.count < 8) {
                    return '';
                }
                return d.data.percentage + '%';



            });

    };






    function VerticalBarchart1(options, ChartType) {
        $(options.container).empty();
        if (options) {
            options.container = options.container ? options.container : "body";
            options.width = options.width ? options.width : $(options.container).width() / 3.05;
            options.height = options.height ? options.height : 300;
            options.marginTop = options.marginTop ? options.marginTop : 30;
            options.marginBottom = options.marginBottom ? options.marginBottom : 40;
            options.marginRight = options.marginRight ? options.marginRight : 20;
            options.marginLeft = options.marginLeft ? options.marginLeft : 50;
            options.show_YAxis = options.show_YAxis ? options.show_YAxis : true;
            options.show_YAxisText = options.show_YAxisText ? options.show_YAxisText : true;
            options.rotate_YAxisText = options.rotate_YAxisText ? options.rotate_YAxisText : false;
            options.show_YAxisPath = options.show_YAxisPath ? options.show_YAxisPath : true;
            options.show_YAxisTicks = options.show_YAxisTicks ? options.show_YAxisTicks : true;
            options.rotate_YAxisTicks = options.rotate_YAxisTicks ? options.rotate_YAxisTicks : true;
            options.fontSize_YAxis = options.fontSize_YAxis ? options.fontSize_YAxis : 10;
            options.fontFamily_YAxis = options.fontFamily_YAxis ? options.fontFamily_YAxis : "sans-serif";
            options.show_XAxis = options.show_XAxis ? options.show_XAxis : true;
            options.show_XAxisText = options.show_XAxisText ? options.show_XAxisText : true;
            options.rotate_XAxisText = options.rotate_XAxisText ? options.rotate_XAxisText : true;
            options.show_XAxisPath = options.show_XAxisPath ? options.show_XAxisPath : true;
            options.show_XAxisTicks = options.show_XAxisTicks ? options.show_XAxisTicks : true;
            options.rotate_XAxisTicks = options.rotate_XAxisTicks ? options.rotate_XAxisTicks : false;
            options.fontSize_XAxis = options.fontSize_XAxis ? options.fontSize_XAxis : 10;
            options.fontFamily_XAxis = options.fontFamily_XAxis ? options.fontFamily_XAxis : "sans-serif";
            options.gridx = options.gridx ? options.gridx : false;
            options.gridy = options.gridy ? options.gridy : false;
            options.strokeLineColor = options.strokeLineColor ? options.strokeLineColor : "red"
        }

        margin = { top: options.marginTop, right: options.marginRight, bottom: options.marginBottom, left: options.marginLeft }

        var width = options.width - options.marginRight - options.marginLeft,
            height = options.height - options.marginTop - options.marginBottom;

        var svg = d3.select("body")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.bottom + "," + margin.top + ")");

        if (ChartType == "GoogleCharts") {
            var x0 = d3.scaleBand()
                .rangeRound([0, width - margin.left])
                .paddingInner(0.4);

            var x1 = d3.scaleBand()
                .padding(0.05);

        }

        if (ChartType == "HighCharts") {
            var x0 = d3.scaleBand()
                .rangeRound([0, width - margin.left])
                .paddingInner(0.6);

            var x1 = d3.scaleBand()
                .padding(0.15);

        }

        var y = d3.scaleLinear()
            .rangeRound([height, 0]);

        var z = d3.scaleOrdinal()
            .range(["#0082da", "#06243b"]);

        d3.csv("data1.csv", function (d, i, columns) {
            for (var i = 1, n = columns.length; i < n; ++i) d[columns[i]] = +d[columns[i]];
            return d;
        }, function (error, data) {
            if (error) throw error;

            var keys = data.columns.slice(1);

            var xScale = x0.domain(data.map(function (d) { return d.Month; }));
            x1.domain(keys).rangeRound([0, x0.bandwidth()]);
            var yScale = y.domain([0, 40]);

            svg.append("g")
                .attr("class", "view")
                .append("text")
                .attr("x", 1)
                .attr("y", height + 20)
                .attr("transform", "translate(-30,0)")
                .attr("dy", "0.32em")
                .attr("text-anchor", "start")
                .text("View");


            svg.append("g")
                .append("text")
                .attr("class", "toplabel")
                .attr("x", width / 4 - margin.left)
                .attr("transform", "translate(-7,0)")
                .attr("y", -10)
                .text("Column Simple");
            // Add the X Axis
            var x_g = svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .style("display", function () {
                    return options.show_XAxis ? 'block' : 'none';
                })
                .call(d3.axisBottom(x0).tickSize(0));
            //  X Axis path        
            x_g.selectAll("path")
                .attr("class", "x axispath")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_XAxisPath ? 'block' : 'none';
                });
            //  X Axis Text  
            x_g.selectAll('text')
                .attr('font-weight', 'normal')
                .style("font-size", options.fontSize_XAxis + "px")
                .style("font-family", options.fontFamily_XAxis)
                .style("text-anchor", "start")
                //.attr("dx", ".2em")
                .attr("dy", ".8em")
            // .attr("transform", function () {
            //     return options.rotate_XAxisText ? 'rotate(-120)' : 'rotate(0)';
            // });

            //  X Axis Ticks                   
            x_g.selectAll("line")
                .attr("class", "x axisline")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_XAxisTicks ? 'block' : 'none';
                })
            // .attr("transform", function () {
            //     return options.rotate_XAxisTicks ? 'rotate(-60)' : 'rotate(0)';
            // });

            // Add the Y Axis
            var y_g = svg.append("g")
                .style("display", function () {
                    return options.show_YAxis ? 'block' : 'none';
                })
                .call(d3.axisLeft(y).ticks(5).tickFormat(function (d) { return d + "lbs" }).tickSize(-width + margin.right + margin.bottom));


            //  Y Axis path        
            y_g.selectAll("path")
                .attr("class", "y axispath")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_YAxisPath ? 'block' : 'none';
                });
            //  Y Axis Text                   
            y_g.selectAll('text')
                .attr('font-weight', 'normal')
                .style("font-size", options.fontSize_YAxis + "px")
                .style("font-family", options.fontFamily_YAxis)
                .style("display", function () {
                    return options.show_YAxisText ? 'block' : 'none';
                })
            // .attr("transform", function () {
            //     return options.rotate_YAxisText ? 'rotate(-120)' : 'rotate(0)';
            // });
            //  Y Axis Ticks                   
            y_g.selectAll("line")
                .attr("class", "y axisline")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_YAxisTicks ? 'block' : 'none';
                })
            // .attr("transform", function () {
            //     return options.rotate_YAxisTicks ? 'rotate(-60)' : 'rotate(0)';
            // });

            h = $("body").width()
            console.log(h)
            if (ChartType == "GoogleCharts") {
                var size = 18
                svg.selectAll("legend")
                    .data(keys)
                    .enter()
                    .append("rect")
                    .attr("x", h / 4.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 5) })
                    .attr("width", size)
                    .attr("height", size)
                    .style("fill", function (d) { return z(d) })

                // Add one dot in the legend for each name.
                svg.selectAll("legendlabel")
                    .data(keys)
                    .enter()
                    .append("text")
                    .attr("x", h / 4.2 + size * 1.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 5) + (size / 2) })
                    .text(function (d) { return d })
                    .attr("text-anchor", "left")
                    .style("alignment-baseline", "middle")

            }

            if (ChartType == "HighCharts") {
                var size = 20
                svg.selectAll("legend")
                    .data(keys)
                    .enter()
                    .append("rect")
                    .attr("x", h / 4.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 0.5) })
                    .attr("width", size + 1)
                    .attr("height", size - 2)
                    .style("fill", function (d) { return z(d) })

                svg.append("rect")
                    .attr("x", h / 4.35)
                    .attr("y", function (d, i) { return 90 + i * (size + 0.5) })
                    .attr("rx", 10)
                    .attr("rx", "10")
                    .attr("width", 90)
                    .attr("height", 60)
                    .attr("fill", "none")
                    .attr("stroke-width", "0.2px")
                    .attr("stroke", "black");

                // Add one dot in the legend for each name.
                svg.selectAll("legendlabel")
                    .data(keys)
                    .enter()
                    .append("text")
                    .attr("x", h / 4.2 + size * 1.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 2) + (size / 2) })
                    .text(function (d) { return d })
                    .attr("text-anchor", "left")
                    .style("alignment-baseline", "middle")

            }


            svg.append("g")
                .selectAll("g")
                .data(data)
                .enter().append("g")
                .attr("transform", function (d) { return "translate(" + x0(d.Month) + ",0)"; })
                .selectAll("rect")
                .data(function (d) { return keys.map(function (key) { return { key: key, value: d[key] }; }); })
                .enter().append("rect")
                .attr("x", function (d) { return x1(d.key); })
                .attr("y", function (d) { return y(d.value); })
                .attr("width", x1.bandwidth())
                .attr("height", function (d) { return height - y(d.value); })
                .attr("fill", function (d) { return z(d.key); });


        });
    };


    function HorizontalBarchart1(options, ChartType) {
        $(options.container).empty();
        if (options) {
            options.container = options.container ? options.container : "body";
            options.width = options.width ? options.width : $(options.container).width() / 3;
            options.height = options.height ? options.height : 300;
            options.marginTop = options.marginTop ? options.marginTop : 30;
            options.marginBottom = options.marginBottom ? options.marginBottom : 30;
            options.marginRight = options.marginRight ? options.marginRight : 20;
            options.marginLeft = options.marginLeft ? options.marginLeft : 50;
            options.show_YAxis = options.show_YAxis ? options.show_YAxis : true;
            options.show_YAxisText = options.show_YAxisText ? options.show_YAxisText : true;
            options.rotate_YAxisText = options.rotate_YAxisText ? options.rotate_YAxisText : false;
            options.show_YAxisPath = options.show_YAxisPath ? options.show_YAxisPath : true;
            options.show_YAxisTicks = options.show_YAxisTicks ? options.show_YAxisTicks : true;
            options.rotate_YAxisTicks = options.rotate_YAxisTicks ? options.rotate_YAxisTicks : true;
            options.fontSize_YAxis = options.fontSize_YAxis ? options.fontSize_YAxis : 10;
            options.fontFamily_YAxis = options.fontFamily_YAxis ? options.fontFamily_YAxis : "sans-serif";
            options.show_XAxis = options.show_XAxis ? options.show_XAxis : true;
            options.show_XAxisText = options.show_XAxisText ? options.show_XAxisText : true;
            options.rotate_XAxisText = options.rotate_XAxisText ? options.rotate_XAxisText : true;
            options.show_XAxisPath = options.show_XAxisPath ? options.show_XAxisPath : true;
            options.show_XAxisTicks = options.show_XAxisTicks ? options.show_XAxisTicks : true;
            options.rotate_XAxisTicks = options.rotate_XAxisTicks ? options.rotate_XAxisTicks : false;
            options.fontSize_XAxis = options.fontSize_XAxis ? options.fontSize_XAxis : 10;
            options.fontFamily_XAxis = options.fontFamily_XAxis ? options.fontFamily_XAxis : "sans-serif";
            options.gridx = options.gridx ? options.gridx : false;
            options.gridy = options.gridy ? options.gridy : false;
            options.strokeLineColor = options.strokeLineColor ? options.strokeLineColor : "red"
        }

        margin = { top: options.marginTop, right: options.marginRight, bottom: options.marginBottom, left: options.marginLeft }

        var width = options.width - options.marginRight - options.marginLeft,
            height = options.height - options.marginTop - options.marginBottom;

        var svg = d3.select("body")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.bottom + "," + margin.top + ")");

        if (ChartType == "GoogleCharts") {
            var y0 = d3.scaleBand()
                .rangeRound([height, 0])
                .paddingInner(0.1);

            var y1 = d3.scaleBand()
                .padding(0.05);

            var z = d3.scaleOrdinal()
                .range(["#0082da", "#06243b"]);
        }
        if (ChartType == "HighCharts") {
            var y0 = d3.scaleBand()
                .rangeRound([height, 0])
                .paddingInner(0.4);

            var y1 = d3.scaleBand()
                .padding(0.15);

            var z = d3.scaleOrdinal()
                .range(["#06243b", "#0082da"]);

        }

        var x = d3.scaleLinear()
            .rangeRound([0, width]);

        var u = d3.scaleOrdinal()
            .range(["#0082da", "#06243b"]);

        d3.csv("data.csv", function (d, i, columns) {
            for (var i = 1, n = columns.length; i < n; ++i) d[columns[i]] = +d[columns[i]];
            return d;
        }, function (error, data) {
            if (error) throw error;
            var keys = data.columns.slice(1);
            // if (ChartType == "HighChartss") {
            //   var keys = data.columns.slice(1).reverse();
            // }

            y0.domain(data.map(function (d) { return d.Month; }));
            y1.domain(keys).rangeRound([0, y0.bandwidth()]);
            x.domain([0, 40]);




            // Add the X Axis
            var x_g = svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .style("display", function () {
                    return options.show_XAxis ? 'block' : 'none';
                })
                .call(d3.axisBottom(x).tickFormat(function (d) { return d + "lbs" }).tickSize(-width).ticks(3));
            //  X Axis path        
            x_g.selectAll("path")
                .attr("class", "x axispath")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_XAxisPath ? 'block' : 'none';
                });
            //  X Axis Text  
            x_g.selectAll('text')
                .attr('font-weight', 'normal')
                .style("font-size", options.fontSize_XAxis + "px")
                .style("font-family", options.fontFamily_XAxis)
                .style("text-anchor", "start")
                //.attr("dx", ".8em")
                .attr("dy", ".5em")
            // .attr("transform", function () {
            //     return options.rotate_XAxisText ? 'rotate(-120)' : 'rotate(0)';
            // });

            //  X Axis Ticks                   
            x_g.selectAll("line")
                .attr("class", "x axisline")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_XAxisTicks ? 'block' : 'none';
                })
            // .attr("transform", function () {
            //     return options.rotate_XAxisTicks ? 'rotate(-60)' : 'rotate(0)';
            // });

            // Add the Y Axis
            var y_g = svg.append("g")
                .style("display", function () {
                    return options.show_YAxis ? 'block' : 'none';
                })
                .call(d3.axisLeft(y0).tickSize(0));


            //  Y Axis path        
            y_g.selectAll("path")
                .attr("class", "y axispath")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_YAxisPath ? 'block' : 'none';
                });
            //  Y Axis Text                   
            y_g.selectAll('text')
                .attr('font-weight', 'normal')
                .style("font-size", options.fontSize_YAxis + "px")
                .style("font-family", options.fontFamily_YAxis)
                .style("display", function () {
                    return options.show_YAxisText ? 'block' : 'none';
                })
            // .attr("transform", function () {
            //     return options.rotate_YAxisText ? 'rotate(-120)' : 'rotate(0)';
            // });
            //  Y Axis Ticks                   
            y_g.selectAll("line")
                .attr("class", "y axisline")
                .style("stroke", "#ccc")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none")
                .style("display", function () {
                    return options.show_YAxisTicks ? 'block' : 'none';
                })
            // .attr("transform", function () {
            //     return options.rotate_YAxisTicks ? 'rotate(-60)' : 'rotate(0)';
            // });

            svg.append("g")
                .attr("class", "view")
                .append("text")
                .attr("x", 1)
                .attr("y", height + 20)
                .attr("transform", "translate(-30,0)")
                .attr("dy", "0.32em")
                .attr("text-anchor", "start")
                .text("View");


            svg.append("g")
                .append("text")
                .attr("class", "toplabel")
                .attr("x", width / 4 - margin.left)
                .attr("transform", "translate(-10,0)")
                .attr("y", -10)
                .text("Bar Simple");

            h = $("body").width()
            console.log(h)
            if (ChartType == "GoogleCharts") {
                var size = 18
                svg.selectAll("legend")
                    .data(keys)
                    .enter()
                    .append("rect")
                    .attr("x", h / 4.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 5) })
                    .attr("width", size)
                    .attr("height", size)
                    .style("fill", function (d) { return z(d) })

                // Add one dot in the legend for each name.
                svg.selectAll("legendlabel")
                    .data(keys)
                    .enter()
                    .append("text")
                    .attr("x", h / 4.2 + size * 1.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 5) + (size / 2) })
                    .text(function (d) { return d })
                    .attr("text-anchor", "left")
                    .style("alignment-baseline", "middle")

            }
            if (ChartType == "HighCharts") {
                var size = 20
                svg.selectAll("legend")
                    .data(keys)
                    .enter()
                    .append("rect")
                    .attr("x", h / 4.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 0.5) })
                    .attr("width", size + 1)
                    .attr("height", size - 2)
                    .style("fill", function (d) { return u(d) })

                svg.append("rect")
                    .attr("x", h / 4.35)
                    .attr("y", function (d, i) { return 90 + i * (size + 0.5) })
                    .attr("rx", 10)
                    .attr("rx", "10")
                    .attr("width", 90)
                    .attr("height", 60)
                    .attr("fill", "none")
                    .attr("stroke-width", "0.2px")
                    .attr("stroke", "black");

                // Add one dot in the legend for each name.
                svg.selectAll("legendlabel")
                    .data(keys)
                    .enter()
                    .append("text")
                    .attr("x", h / 4.2 + size * 1.2)
                    .attr("y", function (d, i) { return 100 + i * (size + 2) + (size / 2) })
                    .text(function (d) { return d })
                    .attr("text-anchor", "left")
                    .style("alignment-baseline", "middle")

            }



            svg.append("g")
                .selectAll("g")
                .data(data)
                .enter().append("g")
                .attr("transform", function (d) { return "translate(0," + y0(d.Month) + ")"; })
                .selectAll("rect")
                .data(function (d) { return keys.map(function (key) { return { key: key, value: d[key] }; }); })
                .enter().append("rect")
                .attr("y", function (d) { return y1(d.key); })
                .attr("data-index", function (d, i) {
                    return d.value;
                })
                .attr("height", y1.bandwidth())
                .attr("width", function (d) { return x(d.value); })
                .attr("fill", function (d) { return z(d.key); });


        });
    };
    Piechart1(option, "GoogleCharts");
    Piechart1(option, "HighCharts");
    VerticalBarchart1(option, "GoogleCharts");
    VerticalBarchart1(option, "HighCharts");
    HorizontalBarchart1(option, "GoogleCharts");
    HorizontalBarchart1(option, "HighCharts");

});
